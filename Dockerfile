FROM node:alpine

RUN npm install -g nodemon

ADD . /opt/app

WORKDIR /opt/app

CMD ["/usr/local/bin/yarn", "start"]
