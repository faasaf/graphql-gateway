import { ApolloServer, gql } from "apollo-server-express";
import cookieParser from "cookie-parser";
import cors from "cors";
import express, { Request, Response } from "express";
import fs from "fs";
import { isScalarType } from "graphql";
import { GraphQLJSONObject } from "graphql-type-json";
import fetch from "node-fetch";
import yaml from "yaml";
import createResolvers from "./createResolversFromTypeDefs";
import { Map } from "./mapping";
import transformResponse from "./transformResponse";

const GRAPHQL_SCHEMA_PATH = process.env.GRAPHQL_SCHEMA_PATH || "/config/schema.graphql";
const MAPPING_PATH = process.env.MAPPING_PATH || "/config/mapping.yml";
const GRAPHQL_PORT = process.env.GRAPHQL_PORT || 4000;
const DEFAULT_BACKEND = process.env.DEFAULT_BACKEND || "http://127.0.0.1:8080/function";

const JSONObject = {
  ...GraphQLJSONObject,
  name: "JSON",
};

const resolvers = {
  JSON: JSONObject,
};

const log = (msg: string) => {
  console.log(`${new Date().toISOString()} [graphql-gateway]: ${msg}`);
};

const logerror = (msg: string) => {
  console.error(`${new Date().toISOString()} [graphql-gateway]: ${msg}`);
};

fs.readFile(GRAPHQL_SCHEMA_PATH, (err, data) => {
  if (err) {
    logerror(JSON.stringify(err));
    return;
  }

  let maps = {
    default: DEFAULT_BACKEND,
  } as Record<string, Map | string>;

  if (fs.existsSync(MAPPING_PATH)) {
    try {
      const mapRaw = fs.readFileSync(MAPPING_PATH, "utf-8");
      const loadedYaml = yaml.parse(mapRaw);
      if (isScalarType(loadedYaml)) {
        logerror(`Map is not an object. Got "${loadedYaml}".`);
        return;
      }
      maps = loadedYaml as Record<string, Map | string>;
      if (!maps.default) {
        maps.default = DEFAULT_BACKEND;
      }
    } catch (e) {
      logerror(`Error reading map config. "${JSON.stringify(e)}"`);
    }
  }

  const typeDefs = gql`
    ${data}
  `;

  const app = express();
  let currentResponse: Response | null = null;
  let currentRequest: Request | null = null;
  async function startApolloServer() {
    app.use(cors());
    app.use(cookieParser());
    app.use((req, res, next) => {
      currentResponse = res;
      currentRequest = req;
      next();
    });
    await server.start();
    server.applyMiddleware({ app });
    new Promise((resolve: any) => app.listen({ port: GRAPHQL_PORT, path: "/" }, resolve));
    log(`🚀 Server ready at http://localhost:${GRAPHQL_PORT}${server.graphqlPath}`);
    return { server, app };
  }

  const server = new ApolloServer({
    typeDefs,
    resolvers: {
      ...createResolvers({
        typeDefs,
        maps,
        callback: (url, options, args, name) => {
          return fetch(url, {
            ...options,
            ...{
              headers: currentRequest?.headers as any,
              body: JSON.stringify(Object.values(args)[0]),
            },
          })
            .then((response) => {
              if (currentResponse) {
                const setCookie = response.headers.get("set-cookie");
                if (setCookie) {
                  currentResponse.append("Set-Cookie", setCookie);
                }
                const rawCookies = response.headers.get("cookie");
                if (rawCookies) {
                  currentResponse.append("Cookie", rawCookies);
                }
              }

              return transformResponse(response.headers.get("Content-Type") || "text/plain", response).then((r) => {
                return r;
              });
            })
            .catch((error) => {
              const errorJson = JSON.stringify(error);
              logerror(errorJson);
              return errorJson;
            });
        },
        ...resolvers,
      }),
    },
  });

  return startApolloServer();
});
