import { RequestInfo, RequestInit } from "node-fetch";

export interface Map {
  url: RequestInfo;
  options?: RequestInit;
  path?: string
}

export const getMap = (name: string, maps: Record<string, Map | string>): Map | string => {
  if (name in maps) {

    if (typeof maps[name] !== 'string') {
      const map: Map = maps[name] as Map;
      if (!map.url) {
        map.url = typeof maps.default === 'string' ? maps.default : maps.default.url
      }
    }

    return maps[name];
  }

  return maps.default;
};
