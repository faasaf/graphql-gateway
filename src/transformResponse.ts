import NestedError from "nested-error";
import { Response } from "node-fetch";

export interface Transformers {
  [key: string]: (response: Response, transformers: Transformers) => Promise<any>;
}

class MessageError extends NestedError {
  public message: string;
  constructor(m: string, err: Error) {
    super(err);
    this.message = m;
    Object.setPrototypeOf(this, MessageError.prototype);
  }
}

export const defaultTransformers: Transformers = {
  "text/plain": async (response) => {
    return response.text();
  },
  "text/html": async (response) => {
    return transformResponse("text/plain", response);
  },
  "html/text": async (response) => {
    return transformResponse("text/html", response);
  },
  "application/json": async (response) => {
    return transformResponse("text/plain", response).then((text) => {
      try {
        return JSON.parse(text);
      } catch (ex) {
        throw new MessageError(`Could not parse "${text}" as JSON.`, ex);
      }
    });
  },
};

export const transformResponse = (type: string, response: Response, transformers?: Transformers) => {
  const t = transformers ? transformers : defaultTransformers;
  return t[type.split(";")[0].trim()](response, t);
};

export default transformResponse;
