import { DocumentNode, ObjectTypeDefinitionNode } from "graphql";
import "isomorphic-fetch";
import createResolver, { ResolverOptions } from "./createResolver";
import { getMap, Map } from "./mapping";

export interface CreateResolversArgs<T> {
  typeDefs: DocumentNode;
  maps: Record<string, Map | string>;
  callback: (url: string, options: ResolverOptions, args: any, name: string) => Promise<T>;
}

export default <T = any>({ typeDefs, maps, callback }: CreateResolversArgs<T>) => {
  const resolvers: Record<string, Record<string, Function>> = {};

  typeDefs.definitions
    .filter(n => n.kind === "ObjectTypeDefinition")
    .filter((n) => {
      const name = (n as ObjectTypeDefinitionNode).name.value;
      return (name === "Query" || name === "Mutation");
    })
    .forEach((d: any) => {
      d.fields.forEach((field: any) => {
        const functionName = field.name.value;
        if (!resolvers[d.name.value]) {
          resolvers[d.name.value] = {};
        }
        const map = getMap(field.name.value, maps);
        resolvers[d.name.value][field.name.value] = createResolver<T>(map, functionName, callback);
      });
    });
  return resolvers;
};
