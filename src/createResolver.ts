import { RequestInit } from "node-fetch";
import { Map } from "./mapping";

export type ResolverOptions = RequestInit & {
  credentials: string;
};

const defaultOptions: ResolverOptions = {
  credentials: "same-origin",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  method: "POST",
};

export default <T>(
    mapping: Map | string,
    functionName: string,
    callback: (url: string, options: ResolverOptions, args: any, name: string) => Promise<T>,
  ) =>
  async (parent: any, args: Record<string, any>) => {
    let url: string = mapping as string;
    let extraOptions = {};
    let path = functionName.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();

    if (typeof mapping !== "string") {
      if (mapping.url) {
        url = mapping.url as string;
      }
      extraOptions = mapping.options || {};
      path = mapping.path ? mapping.path : path;
    }

    return await callback(
      `${url}/${path}`,
      {
        ...defaultOptions,
        ...extraOptions,
      },
      args,
      functionName
    );
  };
