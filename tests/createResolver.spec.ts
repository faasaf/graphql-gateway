import { expect } from "chai";
import { RequestInit } from "node-fetch";
import createResolver from "../src/createResolver";

describe("createResolver creates a resolver function with a callback.", () => {
  it("Calls the callback on the correct function name. (mapping object)", async () => {
    const mapping = {
      url: "https://example.com",
      options: {
        method: 'GET'
      },
    };
    const expectedFunctionName = "test-function";
    const inputFunctionName = "testFunction";

    const resolver = createResolver<{
      args: Record<string, any>;
      url: string;
      options: RequestInit;
    }>(mapping, inputFunctionName, (url, options, args) => {
      expect(url).to.be.equal(`${mapping.url}/${expectedFunctionName}`);
      return Promise.resolve({
        args,
        url,
        options,
      });
    });

    const result = await resolver({}, { input: "argument" });
    expect(result.args.input).to.equal("argument");
    expect(result.options.method).to.equal("GET");
  });
  it("Calls the callback on the correct function name. (mapping string)", async () => {
    const mapping = "https://example.com";
    const expectedFunctionName = "test-function";
    const inputFunctionName = "testFunction";

    const resolver = createResolver<{
      args: Record<string, any>;
      url: string;
      options: RequestInit;
    }>(mapping, inputFunctionName, (url, options, args) => {
      expect(url).to.be.equal(`${mapping}/${expectedFunctionName}`);
      return Promise.resolve({
        args,
        url,
        options,
      });
    });

    const result = await resolver({}, { input: "argument" });
    expect(result.args.input).to.equal("argument");
  });
  it("Calls overrides the function name path.", async () => {
    const mapping = {
      url: "https://example.com",
      options: {
        method: 'GET'
      },
      path: "override/path"
    };
    const expectedFunctionName = "override/path";
    const inputFunctionName = "testFunction";

    const resolver = createResolver<{
      args: Record<string, any>;
      url: string;
      options: RequestInit;
    }>(mapping, inputFunctionName, (url, options, args) => {
      expect(url).to.be.equal(`${mapping.url}/${expectedFunctionName}`);
      return Promise.resolve({
        args,
        url,
        options,
      });
    });

    const result = await resolver({}, { input: "argument" });
    expect(result.args.input).to.equal("argument");
    expect(result.options.method).to.equal("GET");
  });
});
