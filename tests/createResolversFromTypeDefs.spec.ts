import { gql } from "apollo-server-core";
import { expect } from "chai";
import createResolvers from "../src/createResolversFromTypeDefs";

describe("Create GraphQL resolvers to proxy to specific URLs.", () => {
  it("resolves query to mapped backend.", async () => {
    const maps = {
      default: "http://example.com/default",
      alternateMappingTest: "http://example.com/alternative",
      mappedWithObjectTest: {
        url: "https://www.google.com",
        options: {
          method: "GET",
        }
      }
    };
    const typeDefs = gql`
      type Query {
        someFunctionMappingTest: String
        alternateMappingTest: String
      }
    `;
    const expectedFunctionName = "some-function-mapping-test";
    const resolvers = createResolvers({
      typeDefs,
      maps,
      callback: (url, options, args) => {
        expect(url).to.equal(`${maps.default}/${expectedFunctionName}`);
        return Promise.resolve({
          url,
          options,
          args,
        });
      },
    });

    expect(resolvers).to.not.be.empty;
    expect(resolvers.Query).to.not.be.empty;
    expect(resolvers.Query.someFunctionMappingTest).to.be.a("function");
    expect(resolvers.Query.alternateMappingTest).to.be.a("function");

    [
      {
        function: "someFunctionMappingTest",
        url: `${maps.default}/some-function-mapping-test`,
      },
      {
        function: "alternateMappingTest",
        url: `${maps.alternateMappingTest}/alternate-mapping-test`,
      },
    ].forEach(async (params) => {
      const response = await resolvers.Query[params.function]({}, { input: "hallo" });
      expect(response).to.not.be.null;
      expect(response.url).to.equal(params.url);
      expect(response.args.input).to.equal("hallo");
    });
  });
});
