# Faas GraphQl Gateway (under active development)

This repository carries the code for a minimal GraphQl to http gateway.
The software reads a configured file path to a GraphQl schema
and maps queries and mutations to http endpoints.

This allows function and microservices developers to define GraphQl schemas
which span accross different backends.

## Usage

```bash
docker build -t graphql-gateway .
docker run --rm -v ${PWD}/config:/config -p 4000:4000 -e DEFAULT_BACKEND=https://some.gateway.dev:8080 graphql-gateway
```

### Environment Variables

- DEFAULT_BACKEND: http://127.0.0.1:8080/function
- GRAPHQL_SCHEMA_PATH: /config/schema.graphql
- GRAPHQL_PORT: 4000
- MAPPING_PATH: /config/mapping.yml

### GraphQl Schema

A schema file has to be provided in the path defined in `GRAPHQL_SCHEMA_PATH`.

```graphql

type Product {
    sku: String!
}

input ProductInput {
    sku: String!
}

input OrderInput {
    page: Int!
    size: Int!
}

type Query {
    product(body: ProductInput!): Product[]
    orders(body: OrdersInput!): Orders[]
}

```

The `body` parameter is mandatory, this variable contains the content which should be forwarded as JSON to the backend.

### Backend Mapping

The only required backend information is the `DEFAULT_BACKEND` variable.
You can, however also provide a yaml file in the path defined in `MAPPING_PATH`.

```yaml
---
default:
  url: http://gateway.openfaas.cluster.local.svc:8080/function # If empty it will be set by DEFAULT_BACKEND environment variable.
  options:
    method: POST
    credentials: "same-origin"
    headers:
      Accept: application/json
      "Content-Type": "application/json"
graphQlExplained:
  url:
  options:
    method: GET
  headers:
    Accept: text/html
unrelatedFunctionName:

justPostSomewhere: http://some.domain.com/path/to/function
```
